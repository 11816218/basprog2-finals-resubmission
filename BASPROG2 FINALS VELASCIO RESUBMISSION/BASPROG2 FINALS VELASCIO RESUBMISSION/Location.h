#pragma once
#include <string>
#include <vector>
#include "Pokemon.h"

using namespace std;

class Location
{
public:
	Location();

	void getLocation(string _locationName, bool _isSafe);
	void getLocationCoordinates(int _xMin, int _xMax, int _yMin, int _yMax);

	string getLocationName();
	
	int getXMin();
	int getXMax();
	
	int getYMin();
	int getYMax();

	bool getSafety();
private:
	string locationName;
	int xMin;
	int xMax;
	int yMin;
	int yMax;
	bool isSafe;
};
#include "Location.h"
#include <string>
#include <iostream>
#include <vector>

using namespace std;

Location::Location()
{
	locationName = "Unknown Location";
	xMin = -100;
	xMax = 100;
	yMin = -100;
	yMax = 100;
	isSafe = false;
}

void Location::getLocation(string _locationName, bool _isSafe)
{
	this->locationName = _locationName;
	this->isSafe = _isSafe;
}

void Location::getLocationCoordinates(int _xMin, int _xMax, int _yMin, int _yMax)
{
	/*this->*/xMin = _xMin;
	/*this->*/xMax = _xMax;
	/*this->*/yMin = _yMin;
	/*this->*/yMax = _yMax;
}

string Location::getLocationName()
{
	return locationName;
}


int Location::getXMin()
{
	return xMin;
}
int Location::getXMax()
{
	return xMax;
}
int Location::getYMin()
{
	return yMin;
}
int Location::getYMax()
{
	return yMax;
}

bool Location::getSafety()
{
	return isSafe;
}


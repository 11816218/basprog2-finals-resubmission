#pragma once
#include <string>
#include <vector>
using namespace std;

class Pokemon
{
public:
	Pokemon();
	Pokemon(string _name, int _level, string _type);
	Pokemon(string _name, int _level, string _type, int _baseHp, int _hp, int baseDamage, int exp, int expToLevel);

	Pokemon *chooseStarter();
	void attackPokemon(Pokemon *enemyPokemon);
	void addExp(Pokemon *enemyPokemon);
	void CheckLevelUp();

	string getName();
	int getLevel();
	string getType();
	int getBaseHp();
	int getHp();
	int getBaseDamage();
	int getExp();
	int getExpToLevel();

	void getStats();
	void pokemonCenter();
private:
	string name;
	int level;
	string type;

	int baseHp;
	int hp;
	int baseDamage;
	int exp;
	int expToLevel;


};
#include "Pokemon.h"
#include <iostream>
#include <time.h>

using namespace std;

Pokemon::Pokemon()
{
	name = "";
	level = 0;
	type = "";
	baseHp = 0;
	hp = 0;
	baseDamage = 0;
	exp = 0;
	expToLevel = 0;
	
}

Pokemon::Pokemon(string _name, int _level, string _type)
{
	this->name = _name;
	this->level = _level;
	this->type = _type;
}

Pokemon::Pokemon(string _name, int _level, string _type, int _baseHp, int _hp, int _baseDamage, int _exp, int _expToLevel)
{
	this->name = _name;
	this->level = _level;
	this->type = _type;
	this->baseHp = _baseHp;
	this->hp = _hp;
	this->baseDamage = _baseDamage;
	this->exp = _exp;
	this->expToLevel = _expToLevel;

}

Pokemon *Pokemon::chooseStarter()
{
	cout << "I have 3 Pokemons on hand right now." << endl << "You can have one and let it be your partner for your journey! Go on, choose!" << endl << endl;
	cout << "[1] Treecko - Grass Type - Level 5" << endl;
	cout << "[2] Torchic - Fire Type - Level 5" << endl;
	cout << "[3] Mudkip - Water Type - Level 5" << endl;

	cout << "Input choice:";
	int starterChoice;
	cin >> starterChoice;
	cout << endl;
	for (int i = 0; i < starterChoice; i++)
	{
		if (starterChoice == 1)
		{
			return new Pokemon("Treecko", 5, "Grass", 59, 59, 19, 0, 39);
		}
		else if (starterChoice == 2)
		{
			return new Pokemon("Torchic", 5, "Fire", 59, 59, 19, 0, 39);
		}
		if (starterChoice == 3)
		{
			return new Pokemon("Mudkip", 5, "Water", 59, 59, 19, 0, 39);
		}
		getStats();
		break;
	}
}

void Pokemon::attackPokemon(Pokemon * enemyPokemon)
{
	cout << enemyPokemon->name << " attacked!" << endl;
	cout << enemyPokemon->name << " took " << baseDamage << " damage!"<< endl << endl;
	enemyPokemon->hp -= baseDamage;
}

void Pokemon::addExp(Pokemon *enemyPokemon)
{
	exp += enemyPokemon->expToLevel;
}

void Pokemon::CheckLevelUp()
{
	if (exp >= expToLevel)
	{
		cout << "Your " << getName() << " has leveled up!" << endl;
		level++;
		hp = (hp * .15) + hp;
		baseHp = (baseHp * .15) + baseHp;
		baseDamage = (baseDamage * .10) + baseDamage;
		expToLevel = (expToLevel * .20) + expToLevel;
		exp = (exp * .20) + exp;

		exp = 0;
	}
	getStats();
}


string Pokemon::getName()
{

	return this->name;
}

int Pokemon::getLevel()
{
	return this->level;
}

string Pokemon::getType()
{
	return this->type;
}


int Pokemon::getBaseHp()
{
	return this->baseHp;
}

int Pokemon::getHp()
{
	return this->hp;
}

int Pokemon::getBaseDamage()
{
	return this->baseDamage;
}

int Pokemon::getExp()
{
	return this->exp;
}

int Pokemon::getExpToLevel()
{
	return this->expToLevel;
}

void Pokemon::getStats()
{
		cout << "Name: " << getName() << endl;
		cout << "Level: " << getLevel() << endl;
		cout << "Type: " << getType() << endl;
		cout << "HP: " << getHp() << "/" << getBaseHp() << endl;
		cout << "Attack: " << getBaseDamage() << endl;
		cout << "EXP: " <<getExp() << "/" << getExpToLevel() << endl << endl;

	system("pause");
	system("cls");
}

void Pokemon::pokemonCenter()
{
	hp = baseHp;
}







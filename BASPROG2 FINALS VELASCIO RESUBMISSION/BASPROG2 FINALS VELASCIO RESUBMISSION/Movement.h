#pragma once

class Movement 
{
public:
	Movement();
	Movement(int _x, int _y);

	void displayXandY();

	void moveRight();
	void moveLeft();
	void moveUp();
	void moveDown();
	
	int getX();
	int getY();

private:
	int x;
	int y;
};
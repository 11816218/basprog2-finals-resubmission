#include "Trainer.h"
#include <iostream>

using namespace std;
Trainer::Trainer()
{
	playerName = "";
}

Trainer::Trainer(string _playerName)
{
	this->playerName = _playerName;
}

string Trainer::whatsYourName()
{
	cout << "Welcome to the World of Pokemon! What is your name?" << endl;
	cin >> this->playerName;
	cout << "So your name is " << this->playerName << "? Correct? Excellent!" << endl;
	system("pause");
	system("cls");
	return this->playerName;
}

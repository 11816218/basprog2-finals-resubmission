#pragma once
#include <string>

using namespace std;

class Trainer
{
public:
	Trainer();

	Trainer(string _playerName);

	string whatsYourName();
private:
	string playerName;
};
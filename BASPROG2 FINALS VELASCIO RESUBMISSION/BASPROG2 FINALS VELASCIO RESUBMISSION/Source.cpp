#include <iostream>
#include <string>
#include <vector>
#include "Pokemon.h"
#include "Trainer.h"
#include "Movement.h"
#include "Location.h"
#include <time.h>

using namespace std;

Pokemon *chooseStarter()
{
	cout << "I have 3 Pokemons on hand right now." << endl << "You can have one and let it be your partner for your journey! Go on, choose!" << endl << endl;
	cout << "[1] Treecko - Grass Type - Level 5" << endl;
	cout << "[2] Torchic - Fire Type - Level 5" << endl;
	cout << "[3] Mudkip - Water Type - Level 5" << endl;

	cout << "Input choice:";
	int starterChoice;
	cin >> starterChoice;
	cout << endl;
	for (int i = 0; i < starterChoice; i++)
	{
		if (starterChoice == 1)
		{
			return new Pokemon("Treecko", 5, "Grass", 59, 59, 19, 0, 39);
		}
		else if (starterChoice == 2)
		{
			return new Pokemon("Torchic", 5, "Fire", 59, 59, 19, 0, 39);
		}
		if (starterChoice == 3)
		{
			return new Pokemon("Mudkip", 5, "Water", 59, 59, 19, 0, 39);
		}
		Pokemon *starter = new Pokemon();
		starter->getStats();
		break;
	}
}

void checkLocation(Location *map, Movement *moving, int &defaultLocation, int &locations)
{
	for (int i = 0; i < locations; i++)
	{
		if ( (((moving->getX())) >= ((map + i)->getXMin()) && ((moving->getX()) <= ((map + i)->getXMax())))
			&&  (((moving->getY()) >= ((map + i)->getYMin())) && ((moving->getY()) <= ((map + i)->getYMax()))) )
		{
			cout << "You are now at position ";
			moving->displayXandY();
			cout << "You are now located in " << (map + i)->getLocationName();
			cout << endl;

			defaultLocation = i;
			break;
		}
	}
}
//Check the current location that the player is at

void trainerAction(Location *map, Movement *moving, Pokemon *pokemonTeam, int &defaultLocation)
{
	int choice = 0;
	char direction;
	if ((map + defaultLocation)->getSafety() == true)
	{
		cout << "What would you like to do?" << endl;
		cout << "[1] - Move" << "		" << "[2] - Pokemons" << "		" << "[3] - Pokemon Center" << endl;
		cin >> choice;
		switch (choice)
		{
		case 1:
			cout << "Where do you want to go?" << endl;
			cout << "[W] - Up" << "		" << "[S] - Down" << "		" << "[A] - Left" << "		" << "[D] - Right" << endl;
			cin >> direction;
			switch (direction)
			{
			case 'W':
			case 'w':
				moving->moveUp();
				break;
			case 'S':
			case 's':
				moving->moveDown();
				break;
			case 'A':
			case 'a':
				moving->moveLeft();
				break;
			case 'D':
			case 'd':
				moving->moveRight();
				break;
			default:
				cout << "Invalid Input, please input a valid key" << endl;
				system("pause");
				system("cls");
				break;
			}
		case 2:
			pokemonTeam->getStats();
			break;
		case 3:
			cout << "Welcome to the Pokemon Center! Would you like to heal your Pokemon? (y/n)" << endl;
			char decision;
			cin >> decision;
			switch (decision)
			{
			case 'Y':
			case 'y':
				pokemonTeam->pokemonCenter();
				cout << "All of your Pokemon are now fully restored! Please come again soon!" << endl;
				system("pause");
				system("cls");
				break;
			case 'N':
			case 'n':
				cout << "We hope to see you again soon!" << endl;
				system("pause");
				system("cls");
				break;
			default:
				cout << "Invalid input, please input a valid character" << endl;
				system("pause");
				system("cls");
				break;
			}
			break;
		default:
			cout << "Invalid input, please input a valid number" << endl;
			system("pause");
			system("cls");
			break;
		}
	}
	else
	{
		cout << "What would you like to do?" << endl;
		cout << "[1] - Move" << "		" << "[2] - Pokemons" << endl;
		cin >> choice;
		switch (choice)
		{
		case 1:
			cout << "Where do you want to go?" << endl;
			cout << "[W] - Up" << "		" << "[S] - Down" << "		" << "[A] - Left" << "		" << "[D] - Right" << endl;
			cin >> direction;
			switch (direction)
			{
			case 'W':
			case 'w':
				moving->moveUp();
				break;
			case 'S':
			case 's':
				moving->moveDown();
				break;
			case 'A':
			case 'a':
				moving->moveLeft();
				break;
			case 'D':
			case 'd':
				moving->moveRight();
				break;
			default:
				cout << "Invalid Input, please input a valid key" << endl;
				system("cls");
				break;
			}
		case 2:
			pokemonTeam->getStats();
			system("pause");
			system("cls");
			break;
		default:
			cout << "Invalid input, please input a valid number" << endl;
			system("pause");
			system("cls");
			break;
		}
	}
}
//Main decision making of the simulator

Pokemon *initializeWildPokemon()
{
	int randomNum = (rand() % 15) + 1;
	switch (randomNum)
	{
	case 1: return new Pokemon("Zigzagoon", 3, "Normal", 25, 25, 5, 0, 28);
	case 2: return new Pokemon("Wurmple", 3, "Bug", 19, 19, 3, 0, 28);
	case 3: return new Pokemon("Silcoon", 3, "Bug", 30, 30, 0, 0, 28);
	case 4: return new Pokemon("Cascoon", 3, "Bug", 30, 30, 0, 0, 28);
	case 5: return new Pokemon("Poochyena", 5, "Normal", 35, 35, 10, 0, 39);
	case 6: return new Pokemon("Seedot", 4, "Grass", 29, 29, 7, 0, 33);
	case 7: return new Pokemon("Lotad", 5, "Water", 27, 27, 9, 0, 39);
	case 8: return new Pokemon("Ralts", 6, "Psychic", 31, 31, 11, 0, 44);
	case 9: return new Pokemon("Taillow", 6, "Flying", 33, 33, 13, 0, 44);
	case 10: return new Pokemon("Wingull", 6, "Flying", 28, 28, 10, 0, 44);
	case 11: return new Pokemon("Shroomish", 8, "Grass", 35, 35, 18, 0, 51);
	case 12: return new Pokemon("Slakoth", 9, "Normal", 40, 40, 18, 0, 55);
	case 13: return new Pokemon("Pikachu", 9, "Electric", 36, 36, 20, 0, 55);
	case 14: return new Pokemon("Shedinja", 9, "Bug", 39, 39, 15, 0, 55);
	case 15: return new Pokemon("Geodude", 9, "Rock", 50, 50, 20, 0, 55);
	}
}

void pokemonEncounter(Location *map, Movement *moving, Pokemon *pokemonTeam, int &defaultLocation)
{
	srand(time(0));
	int choice;
	int captureChance = rand() % 10 + 1;
	if ((map + defaultLocation)->getSafety() == false)
	{
		Pokemon *wildPokemonTeam = new Pokemon();
		wildPokemonTeam = initializeWildPokemon();
		Pokemon *capturedPokemon = new Pokemon();
		
		
		cout << "You have encountered a wild " << wildPokemonTeam->getName() << "!" << endl;
		cout << "Name: " << wildPokemonTeam->getName() << endl;
		cout << "Level: " << wildPokemonTeam->getLevel() << endl;
		cout << "Type: " << wildPokemonTeam->getType() << endl;
		cout << "HP: " << wildPokemonTeam->getHp() << "/" << wildPokemonTeam->getBaseHp() << endl;
		cout << "Attack: " << wildPokemonTeam->getBaseDamage() << endl;
		cout << "EXP: " << wildPokemonTeam->getExp() << "/" << wildPokemonTeam->getExpToLevel() << endl << endl;
		system("pause");
		system("cls");
		while (true)
		{
			cout << "What will you do?" << endl;
			cout << "[1] - Battle		[2] - Capture		[3] - Run Away" << endl;
			cin >> choice;
			switch (choice)
			{
			case 1:
				while (wildPokemonTeam->getHp() > 0 && pokemonTeam->getHp() > 0)
				{
					wildPokemonTeam->attackPokemon(pokemonTeam);
					pokemonTeam->attackPokemon(wildPokemonTeam);
					system("pause");
					system("cls");
				}
				if (wildPokemonTeam->getHp() <= 0)
				{
					cout << "Congrats! You have defeated the wild " << wildPokemonTeam->getName() << "!" << endl;
					pokemonTeam->addExp(wildPokemonTeam);
					pokemonTeam->CheckLevelUp();
				}
				if (pokemonTeam->getHp() <= 0)
				{
					cout << "Oh no! Your Pokemon Fainted!" << endl;
					cout << "." << endl << ".." << endl << "..." << endl << "You Blacked out!" << endl;
					system("pause");
					system("cls");
					map->getLocationCoordinates(0, 0, 0, 0);
					moving->getX();
					moving->getY();
				}
				break;
			case 2:
				cout << "You threw a Pokeball at the wild " << wildPokemonTeam->getName() << "!";
				for (int i = 0; i < captureChance; i++)
				{
					cout << "." << endl;
					system("pause");
					cout << ".." << endl;
					system("pause");
					cout << "..." << endl;
					if (captureChance <= 3)
					{
						cout << "You have captured the wild " << wildPokemonTeam->getName() << "!" << endl;
						capturedPokemon = wildPokemonTeam;
						pokemonTeam = capturedPokemon;
						system("pause");
						system("cls");
					}
					else
					{
						cout << wildPokemonTeam->getName() << " got out of the ball!" << endl;
						system("pause");
						system("cls");
					}
					break;
				}
				break;
			case 3:
				cout << "You have run away!" << endl;
				trainerAction(map, moving, pokemonTeam, defaultLocation);
				break;
			}
			break;
		}
	}
}

void initializeLocations(Location *map, int locations)
{
	map[locations];

	(map + 0)->getLocation("Littleroot Town", true);
	(map + 0)->getLocationCoordinates(-2, 2, -2, 2);

	(map + 1)->getLocation("Route 101", false);
	(map + 1)->getLocationCoordinates(3, 5, 3, 5);

	(map + 2)->getLocation("Petalburg City", true);
	(map + 2)->getLocationCoordinates(6, 9, 6, 9);

	(map + 3)->getLocation("Petalburg Woods", false);
	(map + 3)->getLocationCoordinates(10, 12, 10, 12);
}
//Initializations of locations added by the programmer


int main()
{
	srand(time(0));

	Pokemon *pokemonTeam = new Pokemon();
	Pokemon *wildPokemon = new Pokemon();

	string trainerName;
	Trainer *trainer = new Trainer(trainerName);

	Movement *moving = new Movement();

	int defaultLocation = 0;
	int locations = 5;
	Location *map = new Location[locations];

	initializeLocations(map, locations);
	trainer->whatsYourName();
	pokemonTeam = chooseStarter();
	pokemonTeam->getStats();

	while (true)
	{
		checkLocation(map, moving, defaultLocation, locations);
		trainerAction(map, moving, pokemonTeam, defaultLocation);
		pokemonEncounter(map, moving, pokemonTeam, defaultLocation);
	}

	system("pause");
	system("cls");
}
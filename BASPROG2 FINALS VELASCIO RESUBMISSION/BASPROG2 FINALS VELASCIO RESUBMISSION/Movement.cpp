#include "Movement.h"
#include <iostream>

using namespace std;
Movement::Movement()
{
	x = 0;
	y = 0;
}

Movement::Movement(int _x, int _y)
{
	this->x = _x;
	this->y = _y;
}

void Movement::displayXandY()
{
	cout << "(" << x << ", " << y << ")" << endl;
}

void Movement::moveRight()
{
	x++;
}

void Movement::moveLeft()
{
	x++;
}

void Movement::moveUp()
{
	y++;
}

void Movement::moveDown()
{
	y--;
}

int Movement::getX()
{
	return x;
}

int Movement::getY()
{
	return y;
}



